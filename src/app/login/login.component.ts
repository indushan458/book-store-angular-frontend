import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr'
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  constructor(private builder: FormBuilder, private toastr: ToastrService, private service: AuthService,
    private router: Router) {
      sessionStorage.clear();

  }
  result: any;

  loginform = this.builder.group({
    email: this.builder.control('', Validators.required),
    password: this.builder.control('', Validators.required)
  });

  proceedlogin() {
    if (this.loginform.valid) {
      this.service.Login(this.loginform.value).subscribe(item => {
        this.result = item;

        // console.log(this.result.data?.token);
        if (this.result.success == true) {
          sessionStorage.setItem('token',this.result.data.token);
          sessionStorage.setItem('isloggedin','true');
          sessionStorage.setItem('role',this.result.data.role);
          this.router.navigate(['']);
        }else if(this.result.message == 'user_inactive'){
          this.toastr.error('Please contact Admin', 'InActive User');
        } else {
          this.toastr.error('Invalid credentials');
        }
      });
    } else {
      this.toastr.warning('Please enter valid data.')
    }
  }
}
