import { Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../service/auth.service';
import {MatGridListModule} from '@angular/material/grid-list'; 
import { FormBuilder, Validators } from '@angular/forms';
import {MatIconModule} from '@angular/material/icon'; 

export interface Book {
  color: string;
  cols: number;
  rows: number;
  text: string;
}


@Component({
  selector: 'grid-list-dynamic-example',
  templateUrl: './home.component.html',

})
export class HomeComponent {
  searchkeyword : String ="";
  books:any;
  constructor(private service:AuthService,private builder: FormBuilder,private toastr: ToastrService,private router: Router) {}
  ngOnInit() {
      this.service.getBooks()
        .subscribe(response => {
          this.books = response;
          console.log(this.books);
        });

        
  }

  searchform = this.builder.group({
    searchkeyword: this.builder.control('', Validators.required),
  });
  search() {
    if (this.searchform.valid) {
      this.service.searchBooks(this.searchform.value.searchkeyword).subscribe(response => {
        this.toastr.success('Get search result','Filterd search result')
        this.books = response;
      });
    } else {
      this.toastr.warning('Please enter valid data.')
    }
  }

  resetSearch() {
    this.service.getBooks()
    .subscribe(response => {
      this.books = response;
    });
  }
}