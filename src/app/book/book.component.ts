import { Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BookaddpopupComponent } from '../bookaddpopup/bookaddpopup.component';
import { AuthService } from '../service/auth.service';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent {

  constructor(private service: AuthService,private toastr:ToastrService,private router: Router, private dialog: MatDialog) {
   
    this.LoadBook();

  }
  booklist: any;
  dataSource: any;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  accessdata: any;
  haveedit = false;
  haveadd = false;
  havedelete = false;

  ngAfterViewInit(): void {

  }
  LoadBook() {
    this.service.GetAllBooks().subscribe(res => {
      this.booklist = res;
      this.dataSource = new MatTableDataSource(this.booklist.data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.haveadd=true;
      this.haveedit=true;
    });
  }

  displayedColumns: string[] = ['bk_title', 'author_name', 'bk_edition','bk_isbn'];

  addbook() {
    this.OpenAddBookDialog('1000ms', '600ms');
  }

  OpenAddBookDialog(enteranimation: any, exitanimation: any) {
    const popup = this.dialog.open(BookaddpopupComponent, {
      enterAnimationDuration: enteranimation,
      exitAnimationDuration: exitanimation,
      width: '30%',
    });
    popup.afterClosed().subscribe(res => {
      this.LoadBook();
    });
  }
}
