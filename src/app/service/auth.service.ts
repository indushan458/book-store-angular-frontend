import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http:HttpClient) { 

  }
  apiurl='http://localhost:8000/api/';

  RegisterUser(inputdata:any){
    return this.http.post(this.apiurl+'register',inputdata)
  }

  Login(inputdata:any){
    return this.http.post(this.apiurl+'login',inputdata);
  }

  GetUserbyId(id:any){
    let auth_token = sessionStorage.getItem('token');

    const headers = new HttpHeaders({

        'Content-Type': 'application/json',
        'Authorization': `Bearer ${auth_token}`
      });
 
    const requestOptions = { headers: headers };

    return this.http.get(this.apiurl+'authors/'+id,requestOptions);
  }

  updateAuthorStatus(id:any,inputdata:any){
    let auth_token = sessionStorage.getItem('token');

    const headers = new HttpHeaders({

        'Content-Type': 'application/json',
        'Authorization': `Bearer ${auth_token}`
      });
 
    const requestOptions = { headers: headers };

    return this.http.post(this.apiurl+'updateAuthorStatus/',inputdata,requestOptions);
  }
  
  isloggedin(){
    return sessionStorage.getItem('isloggedin')=='true'?true:false;
  }
  getrole(){
    return sessionStorage.getItem('role')!=null?sessionStorage.getItem('role')?.toString():'';
  }
  GetAllAuthors(){
    let auth_token = sessionStorage.getItem('token');

    const headers = new HttpHeaders({

        'Content-Type': 'application/json',
        'Authorization': `Bearer ${auth_token}`
      });
 
    const requestOptions = { headers: headers };

    return this.http.get('http://localhost:8000/api/authors', requestOptions );
  }

  GetAllBooks(){
    let auth_token = sessionStorage.getItem('token');

    const headers = new HttpHeaders({

        'Content-Type': 'application/json',
        'Authorization': `Bearer ${auth_token}`
      });
 
    const requestOptions = { headers: headers };

    return this.http.get('http://localhost:8000/api/getBooksByAuthor', requestOptions );
  }

  getBooks(){
    return this.http.get('http://localhost:8000/api/books');
  }

  searchBooks(keyword:any){
    return this.http.get('http://localhost:8000/api/books?search='+keyword);
  }

  addBook(inputdata:any,file: File){
    let auth_token = sessionStorage.getItem('token');

    const headers = new HttpHeaders({

        'Authorization': `Bearer ${auth_token}`,
         reportProgress: 'true',
         responseType: 'json'
      });
 
    const requestOptions = { headers: headers };

    const formData: FormData = new FormData();

    formData.append('file', file);
    formData.append('inputdata', JSON.stringify(inputdata));

    return this.http.post(this.apiurl+'books.create',formData,requestOptions);
    
  }
}
