import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms'
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../service/auth.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { FileUploadService } from '../service/file-upload.service';

@Component({
  selector: 'app-updatepopup',
  templateUrl: './bookaddpopup.component.html',
  styleUrls: ['./bookaddpopup.component.css']
})
export class BookaddpopupComponent implements OnInit {

  constructor(private builder: FormBuilder, private service: AuthService, private toastr: ToastrService,
    private dialogref: MatDialogRef<BookaddpopupComponent>, @Inject(MAT_DIALOG_DATA) public data: any,private uploadService: FileUploadService) {
  }
  ngOnInit(): void {

  }
  rolelist: any;
  editdata: any;

  currentFile?: File;
  progress = 0;
  message = '';

  fileName = 'Select File';
  fileInfos?: Observable<any>;

  bookform = this.builder.group({
    bk_title: this.builder.control('', Validators.required),
    bk_edition: this.builder.control('', Validators.required),
    bk_isbn: this.builder.control('', Validators.required),
    fileInput: this.builder.control('', Validators.required),
  });

  AddBook() {
    if (this.bookform.valid) {
      if (this.currentFile) {
        this.service.addBook(this.bookform.value,this.currentFile).subscribe(res => {
          this.toastr.success('Book added Successfully.');
          this.dialogref.close();
        });
      }else{
        this.toastr.warning('Please add a cover image')
      }
    } else {
      this.toastr.warning('Please enter valid data.')
    }
  }

  selectFile(event: any): void {
    if (event.target.files && event.target.files[0]) {
      const file: File = event.target.files[0];
      this.currentFile = file;
      this.fileName = this.currentFile.name;
    } else {
      this.fileName = 'Select File';
    }
  }
}
