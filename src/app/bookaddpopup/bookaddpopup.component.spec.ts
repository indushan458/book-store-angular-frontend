import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookaddpopupComponent } from './bookaddpopup.component';

describe('BookaddpopupComponent', () => {
  let component: BookaddpopupComponent;
  let fixture: ComponentFixture<BookaddpopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookaddpopupComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BookaddpopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
