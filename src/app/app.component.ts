import { Component,DoCheck } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { AuthService } from './service/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements DoCheck {
  constructor(private service: AuthService,private route:Router,private tostr:ToastrService){
    let role=sessionStorage.getItem('role');
    if(role=='admin'){
      this.isadmin=true;
    }
  }

  title = 'authentication';
  isadmin=false;
  isMenuVisible=false;
  isloggedin=this.service.isloggedin();

  ngDoCheck(): void {
    let currentroute = this.route.url;
    let role=sessionStorage.getItem('role');
    if (currentroute == '/login' || currentroute == '/register') {
      this.isMenuVisible = false
    } else {
      this.isMenuVisible = true
    }

    if (role == 'admin') {
      this.isadmin = true;
    }else{
      this.isadmin = false;
    }

    this.isloggedin=this.service.isloggedin();
  }

  logout() {
    sessionStorage.clear(); 
    sessionStorage.setItem('isloggedin','false');
    this.route.navigate(['']);
    this.tostr.warning('Logout successfully.');
  }
}
